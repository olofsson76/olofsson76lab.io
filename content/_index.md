On this site we host all of the information and material pertaining to the workshops given by AST member Pontus Olofsson and his team.

- Access material from previous workshops [here](https://olofsson76.gitlab.io/previous/)
- See list of upcoming workshops [here](https://olofsson76.gitlab.io/upcoming/)
- Read more about our SERVIR project [here](https://olofsson76.gitlab.io/page/about/)


Next workshop is on Friday June 25, 2021, 9-11 GMT/5-7 ET; it's a handson exercise where we will analyze sample results collected under simple random sampling (SRS) and stratified random sampling (STR). We will  construct estimators for estimation of areas and map accuracy with confidence intervals. 

We will use the OpenMRV materials (Olofsson, P. 2021. *Analysis of sample data collected under stratified random sampling*. ©World Bank): 
- the exercise for the analysis of sample results collected under **SRS/SYS** are located [here](https://github.com/openmrv/MRV/blob/main/Formatted/Modules_3/4_analysis_SRS_SYS.md). To get started, open this [spreadsheet](https://docs.google.com/spreadsheets/d/1Ff_vONMTiMxld2pPfgTsrH9DjKM5a5DXxB5kFO6Jaf4/edit?usp=sharing) and click "File" > "Make a copy"
- the exercise for the analysis of sample results collected under **STR** are located [here](https://github.com/openmrv/MRV/blob/main/Formatted/Modules_3/4_analysis_STR.md). To get started, open this [spreadsheet](https://docs.google.com/spreadsheets/d/1Xpi2rCGrtEF7cxYi_cu6CMmV75cYG5ECRMbBkrwh3EQ/edit?usp=sharing) and click "File" > "Make a copy"


**This is the permanent zoom link for the Friday morning workshops: [click here](https://bostonu.zoom.us/j/93283756739?pwd=VU0rSjZKeUQ0alRXVExkYThKc2FXUT09) or copy and paste: https://bostonu.zoom.us/j/93283756739?pwd=VU0rSjZKeUQ0alRXVExkYThKc2FXUT09** 

*Last updated Thursday June 17, 2021 at 15:07 Eastern Time*  



