---
title: 8. Analysis (exercise)
subtitle: June 25, 2021, 9-11 AM GMT/5-7 AM ET
date: 2021-06-25
---


**Objective:** Next workshop is on Friday June 18, 2021, 9-11 GMT/5-7 ET; it's a handson exercise where we will analyze sample results collected under simple random sampling and stratified random sampling. We will  construct estimators for estimation of areas and map accuracy with confidence intervals. 

We will use the OpenMRV materials(Olofsson, P. 2021. *Analysis of sample data collected under stratified random sampling*. © World Bank): 
- the exercise for the analysis of sample results collected under **SRS/SYS** are located [here](https://github.com/openmrv/MRV/blob/main/Formatted/Modules_3/4_analysis_SRS_SYS.md); to get started, open this [spreadsheet](https://docs.google.com/spreadsheets/d/1Ff_vONMTiMxld2pPfgTsrH9DjKM5a5DXxB5kFO6Jaf4/edit?usp=sharing) and click "File" > "Make a copy"
- the exercise for the analysis of sample results collected under **STR** are located [here](https://github.com/openmrv/MRV/blob/main/Formatted/Modules_3/4_analysis_STR.md); to get started, open this [spreadsheet](https://docs.google.com/spreadsheets/d/1Xpi2rCGrtEF7cxYi_cu6CMmV75cYG5ECRMbBkrwh3EQ/edit?usp=sharing) and click "File" > "Make a copy"

**Date and time:** June 25, 2021, 9-11 AM GMT/5-7 AM ET  
**Presenter:** Pontus Olofsson  
**Zoom link:** https://bostonu.zoom.us/j/93283756739?pwd=VU0rSjZKeUQ0alRXVExkYThKc2FXUT09
