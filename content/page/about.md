---
title: About Project
subtitle: SERVIR West Africa
comments: false
---

### Team members

- Principal Investigator: Pontus Olofsson, Boston University [olofsson@bu.edu](olofsson@bu.edu)
- Hub Point of Contact: Foster Mensah, University of Ghana [fkmawusi@gmail.com](fkmawusi@gmail.com)
- Co-Investigator: Curtis Woodcock, Boston University [curtis@bu.edu](curtis@bu.edu)
- PhD Student: Katelyn Tarrio, Boston University [ktarrio@bu.edu](ktarrio@bu.edu)


### Abstract
The aim of this project is to provide the SERVIR-West Africa Hub an integrated solution in Google Earth Engine that 1) provides comprehensive support for sample-based estimation of map accuracy and area of land cover and land change; 2) access to an algorithm proven to monitor forest disturbance and degradation in tropical ecosystems; and 3) access to datasets of land cover and land change with a hierarchical classification system that support harmonization to international standards for production to regional maps consistent across countries and biomes. We aim to enhance the capacity of the Hub by transferring tools and knowledge via close collaboration and regional workshops.
