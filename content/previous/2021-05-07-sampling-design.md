---
title: 3. Sampling Design (Webinar)
subtitle: May 7, 2021
date: 2021-05-07
---

**Objective:** A more in-depth discussion of sampling designs. The webinar explains design criteria and considerations for estimation of area and map accuracy. Common sample selections methods and discussed including approaches for determining sample size are reviewed. Advantanges and disadvantages of simple random, systematic, stratified, and more complex design are discussed.  
**Date:** May 7, 2021  
**Presenter:** Pontus Olofsson  
**Slides:** [download PDF](https://drive.google.com/file/d/1_GR5_HtV7L1rTSu-wk8LxC0e70XiOvPe/view?usp=sharing)  
**Recording:** [view online](https://bostonu.zoom.us/rec/share/3ZR34DFa_auPzupLEaHUfU5pR0CD0cny31qXyWUXGlPHqJ6Cbq0k6iNM8STNibH8.iZP7akN7rm4B_LBn) with passcode: ^c5AD%Ww or [download recording](https://drive.google.com/file/d/1_DvFekobaraLEfWeMwUxYyq1xWfqr0AE/view?usp=sharing) (419 MB) 
