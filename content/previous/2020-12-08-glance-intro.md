---
title: 1. Introduction to GLanCE Data (Webinar)
subtitle: December 8, 2020
date: 2020-12-08
---

**Objective:** This 3 hour workshop provides a conceptual background for understanding land cover/change products from the Global Land Cover mapping and Estimation (GLanCE) project. It includes demonstrations on how to use open-source tools developed in Google Earth Engine to interact with these products to explore change information and extract land cover maps for specific dates.
**Date:** December 8, 2020  
**Presenter:** Katelyn Tarrio  
**Slides:** [download PDF](https://drive.google.com/drive/folders/159uiCLf7CokU4Dtaze-np-2U-UyuPW_A?usp=sharing)
**Recording:** [view online](https://bostonu.zoom.us/rec/share/b_DCVPJ_JSq1fh2Qyan2qrNX0YZp3Lvr8ioyMT7tTJEncCbx8piGmJ7k1i7t7QTu.GivyUbFooUSf4I3O?startTime=1607433627000)
