
---
title: 6. Response Design (Exercise)
subtitle: May 28, 2021, 9-11 AM GMT/5-7 AM ET
date: 2021-05-28
---

**Objective:** This webinar was a hands-on exercise featuring the Collect Earth Online tool with Karis Tenneson from SERVIR Amazonia/SIG. Collect Earth Online is an online tool for collecting of reference observations in satellite data at sample locations without having to download any data. The tool has been used in other Hubs as well. 
**Date:** May 28, 2021  
**Presenter:** Pontus Olofsson with Karis Tenneson from SERVIR Amazonia/SIG  
**Recording:** [view online](https://bostonu.zoom.us/rec/share/xbtx594dgvEE7vMvYJY9Utg_DRZ6dgBCTRaAdO-Cu90tt3aqJWV7GPyowbRK7y2T.vqAfrvfY3ut9IRi9)


