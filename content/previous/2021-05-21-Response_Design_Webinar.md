---
title: 5. Response Design (Webinar/Exercise)
subtitle: May 21, 2021, 9-11 AM GMT/5-7 AM ET
date: 2021-05-21
---

**Objective:** This webinar discusses aspects of observing and recording reference conditions on the land surface. Terms related to response design are defined such as reference data and reference observations, and tools that leverage cloud computing for collection of observations in satellite data are presented. The webinar illustrates how to draw a sample under stratified random sampling for an arbitraty study area in AREA2.  
**Date:** May 21, 2021  
**Presenter:** Pontus Olofsson  
**Slides:** [download PDF](https://drive.google.com/file/d/17QZztJFxGE6SRnSKiBEJgAj3JuC_6a5W/view?usp=sharing)  
**Recording:** [view online](https://bostonu.zoom.us/rec/share/hW7RFnSFmHT7RadBEZ8L2nkslX8Ae2l8gCIi7QLWZ8zui0Fw7_Q5IV2gcXknVBCj.IeLd1PysEK2hGkFI) or [download recording](https://drive.google.com/file/d/1aHwpC_GmmvLjqnEJniPAGsBueizto0LA/view?usp=sharing) (536 MB) 


