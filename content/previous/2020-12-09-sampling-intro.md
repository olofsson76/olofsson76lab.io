---
title: 2. Introduction to sampling-based estimaton of area and map accuracy (Webinar)
subtitle: December 9, 2020
date: 2020-12-09
---

**Objective:** This 2 hour webinar provides an introduction to sampling and inference techniques in a geography context. It explains the ratioanle and motivation for such tecnhiques when working with remote sensing-based maps, and introduces participants to the concept of sampling design, response design and analysis for estimation of area and map accuracy.  
**Date:** December 9, 2020  
**Presenter:** Pontus Olofsson  
**Slides:** [download PDF](https://drive.google.com/file/d/1vMPV7rKj6-qghZd3aIWFnt8JQtc-1r89/view?usp=sharing)  
**Recording:** [view online](https://bostonu.zoom.us/rec/share/9iG6lFXi0dDkXzh3Js31N4e7oMNIEyorTDffonhsg01P3Ak7SlsEnZNxDHMD3zKT.MdSAMv7cEGshcGEt?startTime=1607523133000) with passcode: 1@5R8*uP or [download recording](https://drive.google.com/file/d/1J0qCYFN-aojwgo3rJgwXde7Zr03SFfKW/view?usp=sharing) (250 MB) 
