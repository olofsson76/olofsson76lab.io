---
title: 4. Sampling Design (Exercise)
subtitle: May 14, 2021
date: 2021-05-14
---

**Objective:** This hands-on exercise aims to provide knowledge of how design sampling efforts for estimation of area an map accuracy. Sampling design considerations are reviewed; formulas for sample size determination for simple random, systematic, and stratified random sampling are implemented in spreadsheets; and sample allocation for stratified designs are discussed.  
**Date:** May 14, 2021  
**Presenter:** Pontus Olofsson  
**Instructions:** [SRS/SYS](https://github.com/openmrv/MRV/blob/main/Formatted/Modules_3/1_sampling_design_SRS_SYS.md)  
**Instructions:** [STR](https://github.com/openmrv/MRV/blob/main/Formatted/Modules_3/1_sampling_design_STR.md)  
**Recording:** [view online](https://bostonu.zoom.us/rec/share/UHx0r4p3JrBikIo8k7933f7p6EDdP8CspJp4muO-q_6uJCZ2xagT4GlBG3kV8VRg.WmUGOmzL-MRi6DK8) or [download recording](https://drive.google.com/file/d/1_fZRga-8gFsRzFU855-sgLDtqbI9cq-F/view?usp=sharing) (360 MB) 

