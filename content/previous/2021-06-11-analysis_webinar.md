
---
title: 7. Analysis (Webinar)
subtitle: June 11, 2021, 9-11 AM GMT/5-7 AM ET
date: 2021-06-11
---

**Objective:** This webinar is about the analysis of sample results collected under various designs including simple random and stratified sampling. It also reviews post-straticiation and analysis of sample data collecte under more complicated designs and when estimating accuracy of a map different from the map used to define strata. 
**Date:** June 11, 2021  
**Presenter:** Pontus Olofsson with Karis Tenneson from SERVIR Amazonia/SIG  
**Slides:** [download pdf](https://drive.google.com/file/d/1jiCnYRan5uzK4GuQf0JAlI4CZpG-mRA-/view?usp=sharing)
**Recording:** [view online](https://bostonu.zoom.us/rec/share/sB2eTsHmz11NaDAgFvjRxRSXwfZ8cCjRfI4y3woBwIEkCevZ7YdLxjLeXLvhZpGE.TJKwFiLltmbyL4Bw)

